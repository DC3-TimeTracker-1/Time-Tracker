-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 26 Avril 2016 à 03:54
-- Version du serveur: 5.5.46-0ubuntu0.14.04.2
-- Version de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `ttgroupe1`
--
CREATE DATABASE IF NOT EXISTS `ttgroupe1` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ttgroupe1`;

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `cout` decimal(5,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categorie_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`, `cout`, `user_id`) VALUES
(1, 'Social media', 250.00, 4),
(2, 'Print', 430.00, 2),
(3, 'Webmarketing', 800.00, 4),
(4, 'Référencement naturel', 300.00, 1),
(5, 'Stratégie digitale', 999.99, 2),
(6, 'Evenementiel', 999.99, 3),
(7, 'Photographie', 260.00, 3),
(8, 'Vidéo', 460.00, 2);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_client_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id`, `nom`, `user_id`) VALUES
(1, 'Maison Rozetti', 1),
(2, 'Digital Campus', 2),
(3, 'Médiaveille', 3),
(4, 'Viabilis', 1),
(5, 'Yellow Cake', 4),
(6, 'Nike', 2),
(7, 'Vogue Magazine', 3),
(8, 'Apple', 1),
(9, 'Pulco', 4),
(10, 'Fleury Michon', 2);

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_projet_client_idx` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Contenu de la table `projet`
--

INSERT INTO `projet` (`id`, `nom`, `client_id`) VALUES
(1, 'Recrutement étudiants', 2),
(2, 'Emailing Etudiants', 2),
(3, 'Refonte du site DC', 2),
(4, 'Optimisations clients', 3),
(5, 'Plans de marquage', 3),
(6, 'Intégrations de campagnes adwords', 4),
(7, 'Optimisations', 4),
(8, 'Changements d''URL', 4),
(9, 'Création wireframes', 5),
(10, 'Création de maquettes', 5),
(11, 'Intégration', 5),
(12, 'Kit UI', 5),
(13, 'Arborescence', 1),
(14, 'Création de maquettes', 1),
(15, 'Test de produits', 1),
(16, 'Personnalisatios de sneakers', 6),
(17, 'Catalogue produit', 6),
(18, 'Impression magazines', 7),
(19, 'Rédaction de contenus', 7),
(20, 'Affiches pub iphone 6', 8),
(21, 'Création site web iphone 7', 8),
(22, 'nouvelle gamme de produits', 9),
(23, 'C''est bon le jambon !', 10),
(24, 'Jeux concours "laches ta saucisse"', 10);

-- --------------------------------------------------------

--
-- Structure de la table `tache`
--

DROP TABLE IF EXISTS `tache`;
CREATE TABLE IF NOT EXISTS `tache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `commentaire` text,
  `categorie_id` int(11) NOT NULL,
  `projet_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tache_categorie1_idx` (`categorie_id`),
  KEY `fk_tache_projet1_idx` (`projet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `tache`
--

INSERT INTO `tache` (`id`, `nom`, `commentaire`, `categorie_id`, `projet_id`) VALUES
(1, 'Impression numérique', NULL, 2, 2),
(2, 'Tache 1', NULL, 4, 2),
(3, 'Tache 2', NULL, 7, 3),
(4, 'Tache 3', NULL, 5, 3),
(5, 'Tache 4', NULL, 6, 1),
(6, 'Tache 5', NULL, 3, 1),
(7, 'Tache 6', NULL, 8, 4),
(8, 'Tache 7', NULL, 1, 4),
(9, 'Tache 8', NULL, 3, 5),
(10, 'Tache 9', NULL, 2, 5),
(11, 'Tache 10', NULL, 3, 6),
(12, 'Tache 11', NULL, 5, 6),
(13, 'Tache 12', NULL, 4, 7),
(14, 'Tache 13', NULL, 6, 7),
(15, 'Tache 14', NULL, 5, 8),
(16, 'Tache 15', NULL, 3, 8),
(17, 'Tache 16', NULL, 4, 9),
(18, 'Tache 17', NULL, 5, 9),
(19, 'Tache 19', NULL, 3, 10),
(20, 'Tache 20', NULL, 1, 10);

-- --------------------------------------------------------

--
-- Structure de la table `tache_has_tag`
--

DROP TABLE IF EXISTS `tache_has_tag`;
CREATE TABLE IF NOT EXISTS `tache_has_tag` (
  `tache_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`tache_id`,`tag_id`),
  KEY `fk_tache_has_tag_tag1_idx` (`tag_id`),
  KEY `fk_tache_has_tag_tache1_idx` (`tache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tag`
--

DROP TABLE IF EXISTS `tag`;
CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tag_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `temps`
--

DROP TABLE IF EXISTS `temps`;
CREATE TABLE IF NOT EXISTS `temps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime DEFAULT NULL,
  `tache_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_temps_tache1_idx` (`tache_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `temps`
--

INSERT INTO `temps` (`id`, `date_debut`, `date_fin`, `tache_id`) VALUES
(1, '2016-01-13 13:09:47', '2016-01-13 13:17:36', 1),
(2, '2016-01-15 17:17:56', '2016-01-15 17:23:06', 2),
(3, '2016-01-23 17:28:38', '2016-01-23 17:38:09', 3),
(4, '2016-02-01 19:34:54', '2016-02-01 19:44:51', 4);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `mail`, `mdp`) VALUES
(1, 'clement.salmon@gmail.fr', 'salutlepd'),
(2, 'benjamin.rolland@gmail.com', 'benji'),
(3, 'thomaspierret@gmail.com', 'tomtom'),
(4, 'md.deneef@gmail.com', 'mainadn');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD CONSTRAINT `fk_categorie_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `fk_client_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `projet`
--
ALTER TABLE `projet`
  ADD CONSTRAINT `fk_projet_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `tache`
--
ALTER TABLE `tache`
  ADD CONSTRAINT `fk_tache_categorie1` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tache_projet1` FOREIGN KEY (`projet_id`) REFERENCES `projet` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `tache_has_tag`
--
ALTER TABLE `tache_has_tag`
  ADD CONSTRAINT `fk_tache_has_tag_tache1` FOREIGN KEY (`tache_id`) REFERENCES `tache` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tache_has_tag_tag1` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `tag`
--
ALTER TABLE `tag`
  ADD CONSTRAINT `fk_tag_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `temps`
--
ALTER TABLE `temps`
  ADD CONSTRAINT `fk_temps_tache1` FOREIGN KEY (`tache_id`) REFERENCES `tache` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
