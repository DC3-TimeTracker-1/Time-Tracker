<?php

function getAllProjets() {
    global $connection;

    $query = "
        SELECT *
        FROM projet
        ";
    $stmt = $connection->prepare($query);
    $stmt->execute();

    return $stmt->fetchAll();
}

function getAllProjetsOfClient($client_id) {
    global $connection;

    $query = "
        SELECT *
        FROM projet
		WHERE projet.client_id = :client_id";
    $stmt = $connection->prepare($query);
	$stmt->bindParam(':client_id', $client_id, PDO::PARAM_INT);
    $stmt->execute();

    return $stmt->fetchAll();
}

function getProjet($projet_id) {
    global $connection;
	
	$query = "
        SELECT *
        FROM projet
		WHERE id = :id";
    $stmt = $connection->prepare($query);
	$stmt->bindParam(':id', $projet_id, PDO::PARAM_INT);
    $stmt->execute();
	}

function insertProjet($nom, $client_id) {
    global $connection;
    
    $query = "INSERT INTO projet (nom, client_id)
            VALUES(:nom, :client_id)";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':nom', $nom, PDO::PARAM_STR);
	$stmt->bindParam(':client_id', $client_id, PDO::PARAM_INT);
    $stmt->execute();
}

function deleteProjet($id) {
    global $connection;
    
    $query = "DELETE FROM projet WHERE id = :id";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
}

function updateProjet($nom, $id) {
    global $connection;
    
    $query = "UPDATE projet SET
                nom = :nom
            WHERE id = :id";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':nom', $nom, PDO::PARAM_STR);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
}

