<?php

function getAllTaches() {
    global $connection;

    $query = "
        SELECT *
        FROM tache
        ";
    $stmt = $connection->prepare($query);
    $stmt->execute();

    return $stmt->fetchAll();
}

function getTache($id) {
    global $connection;

    $query = "
        SELECT *
        FROM tache
		WHERE id = :id
        ";
    $stmt = $connection->prepare($query);
	$stmt->bindParam(':id', $id, PDO::PARAM_INT);	
    $stmt->execute();

    return $stmt->fetch();
}

function getAllTachesOfProjet($projet_id) {
    global $connection;
    
    $query = "
				SELECT
					tache.id,
					tache.nom AS tache_nom,
					categorie.nom AS categorie_nom,
					TIME_FORMAT(SEC_TO_TIME(SUM(TIMESTAMPDIFF(SECOND, temps.date_debut, temps.date_fin))), '%H:%i:%s') AS total_tache
				FROM tache
				LEFT JOIN categorie ON categorie.id = tache.categorie_id
				LEFT JOIN temps ON temps.tache_id = tache.id
				WHERE tache.projet_id = :projet_id
                GROUP BY tache.id
	";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':projet_id', $projet_id, PDO::PARAM_INT);
    $stmt->execute();
	
    return $stmt->fetchAll();
}

function insertTache($nom, $projet_id) {
    global $connection;
	
	$query = "INSERT INTO tache (nom, categorie_id, projet_id)
            VALUES(:nom, 1, :projet_id)";
	$stmt = $connection->prepare($query);
	$stmt->bindParam(':nom', $nom, PDO::PARAM_STR);	
    $stmt->bindParam(':projet_id', $projet_id, PDO::PARAM_STR);
	$stmt->execute();
	
	return getTache($connection->lastInsertId());
}

function deleteTache($id) {
    global $connection;
    
    $query = "DELETE * FROM tache WHERE id = :id";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
}

function updateTache($nom, $commentaire, $categorie_id, $projet_id) {
    global $connection;
    
    $query = "UPDATE utilisateur SET
                nom = :nom,
                commentaire = :commentaire,
				categorie_id = :categorie_id,
				projet_id = :projet_id,
            WHERE id = :id";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':nom', $nom, PDO::PARAM_STR);
    $stmt->bindParam(':commentaire', $commentaire, PDO::PARAM_STR);
	$stmt->bindParam(':categorie_id', $categorie_id, PDO::PARAM_INT);
	$stmt->bindParam(':projet_id', $projet_id, PDO::PARAM_INT);
    $stmt->execute();
}