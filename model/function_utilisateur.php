<?php

function getUtilisateurAuth($email, $mot_de_passe) {
    global $connection;
    
    $query = "
        SELECT *
        FROM user
        WHERE mail = :email
        AND mdp = :mot_de_passe;
        ";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':email', $email, PDO::PARAM_STR);
    $stmt->bindParam(':mot_de_passe', $mot_de_passe, PDO::PARAM_STR);
    $stmt->execute();
    
    return $stmt->fetch();
}

function getUtilisateurByEmail($email) {
    global $connection;
    
    $query = "
        SELECT *
        FROM user
        WHERE mail = :email;
        ";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':email', $email, PDO::PARAM_STR);
    $stmt->execute();
    
    return $stmt->fetch();
}

function insertUtilisateur($email, $mdp) {
    global $connection;
    
    $query = "INSERT INTO user (mail, mdp)
            VALUES(:email, :mdp)";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':email', $email, PDO::PARAM_STR);
	$stmt->bindParam(':mdp', $mdp, PDO::PARAM_STR);
    $stmt->execute();
}

function deleteUtilisateur($id) {
    global $connection;
    
    $query = "DELETE FROM user WHERE id = :id";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
}

function updateEmailUtilisateur($email) {
    global $connection;
    
    $query = "UPDATE user SET
                mail = :email
            WHERE id = :id";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':email', $email, PDO::PARAM_STR);
    $stmt->bindParam(':id', $_SESSION['user_id'], PDO::PARAM_INT);
    $stmt->execute();
}

function updateMdpUtilisateur($mdp) {
    global $connection;
    
    $query = "UPDATE user SET
                mdp = :mdp
            WHERE id = :id";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':mdp', $mdp, PDO::PARAM_STR);
    $stmt->bindParam(':id', $_SESSION['user_id'], PDO::PARAM_INT);
    $stmt->execute();
}