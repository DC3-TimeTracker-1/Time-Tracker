<?php
function insertTemps($tache_id) {
    global $connection;	
	
	 $query = "INSERT INTO temps (date_debut, tache_id)
            VALUES(NOW(), :tache_id)";
	$stmt = $connection->prepare($query);
    $stmt->bindParam(':tache_id', $tache_id, PDO::PARAM_INT);
	$stmt->execute();
}

function update_temps_fin($tache_id) {
	 global $connection;
	 
	  $query = "UPDATE temps SET date_fin = NOW()
            WHERE date_fin IS NULL
			AND tache_id = :tache_id";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':tache_id', $tache_id, PDO::PARAM_INT);
    $stmt->execute();
}