<?php

function getAllClients() {
    global $connection;

    $query = "
        SELECT *
        FROM client
        ";
    $stmt = $connection->prepare($query);
    $stmt->execute();

    return $stmt->fetchAll();
}

function insertClient($nom, $user_id) {
    global $connection;
    
    $query = "INSERT INTO client (nom, user_id)
            VALUES(:nom, :user_id)";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':nom', $nom, PDO::PARAM_STR);
	$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->execute();
}


function getClient($client_id) {
    global $connection;
	
	$query = "
        SELECT *
        FROM client
		WHERE id = :id";
    $stmt = $connection->prepare($query);
	$stmt->bindParam(':id', $client_id, PDO::PARAM_INT);
    $stmt->execute();
	}

function deleteClient($id) {
    global $connection;
    
    $query = "DELETE FROM client WHERE id = :id";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
}

function updateClient($nom, $id) {
    global $connection;
    
    $query = "UPDATE client SET
                nom = :nom
            WHERE id = :id";
    $stmt = $connection->prepare($query);
    $stmt->bindParam(':nom', $nom, PDO::PARAM_STR);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
}