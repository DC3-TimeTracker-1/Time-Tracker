<?php

require_once '../../security.php';
require_once '../../database.php';

$mdp = $_POST['new_mdp'];

if ($_POST['old_mdp'] == $user['mdp']) {
	if ($mdp == $_POST['new_mdp2']) {
		updateMdpUtilisateur($mdp);
	}
	else{
		alert("Votre nouveau mot de passe n'est pas identique");
	}
}
else{
	alert("Ancien mot de passe incorrect");
}


header("Location: ../../logout.php");