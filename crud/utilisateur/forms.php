<?php
require_once '../../security.php';
require_once '../../database.php';

$email = $_SESSION['email'];
$get_user = getUtilisateurByEmail($email);


require_once '../../layout/header.php'; ?>

<h1>Paramètres de votre Compte</h1>
<h2>Bonjour, <?php echo $_SESSION['email'] ?></h2>

<section class="update-client">
	<h2>Modifier votre Email</h2>
	<form action="update-email-query.php" method="POST" >

		<p>Email actuel : <?php echo $_SESSION['email'] ?></p>
		
		Nouvel Email
	    <input type="text" name="new_email">

		<input type="submit" value="Modifier">
	</form>
</section>

<section class="update-client">
	<h2>Modifier votre MDP</h2>
	<form action="update-mdp-query.php" method="POST" >

		Mdp Actuel 
		<input type="text" name="old_mdp">
		<br>
		
		Nouveau Mdp
	    <input type="text" name="new_mdp">
	    <br>

	    Confirmer Nouveau Mdp
	    <input type="text" name="new_mdp2">

		<input type="submit" value="Modifier">
	</form>
</section>

<?php require_once '../../layout/footer.php'; ?>