<?php
require_once '../../security.php';
require_once '../../database.php';

$get_clients = getAllClients();


require_once '../../layout/header.php'; ?>

<h1>Paramètres Clients</h1>


<section class="add-client">
	<h2>Créer nouveau Client</h2>
	<form action="create-query.php" method="POST">
		<input type="text" placeholder="Nom Client" name="name-client" required>
		<input type="submit" value="Créer">
	</form>
</section>

<section class="delete-client">
	<h2>Supprimer Client</h2>
	<form action="delete-query.php" method="POST" >

		<select name="delete_client">
			<option value="" disabled selected> Clients</option>
		    <?php foreach ($get_clients as $clients) : ?>
		        <option value="<?php echo $clients['id']; ?>"><?php echo $clients['nom']; ?></option>
		    <?php endforeach; ?>
	    </select>

		<input type="submit" value="Supprimer Client">
	</form>
</section>

<section class="update-client">
	<h2>Modifier Nom Client</h2>
	<form action="update-query.php" method="POST" >

		<select name="id-client">
			<option value="" disabled selected> Clients</option>
		    <?php foreach ($get_clients as $clients) : ?>
		        <option value="<?php echo $clients['id']; ?>"><?php echo $clients['nom']; ?></option>
		    <?php endforeach; ?>
	    </select>

	    <input type="text" name="nom-client">

		<input type="submit" value="Modifier">
	</form>
</section>

<?php require_once '../../layout/footer.php'; ?>