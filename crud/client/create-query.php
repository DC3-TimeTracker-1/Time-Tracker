<?php
require_once '../../security.php';
require_once '../../database.php';

$nom = $_POST['name-client'];
$user_id = $_SESSION['user_id'];

insertClient($nom, $user_id);

header("Location: ../../index.php");