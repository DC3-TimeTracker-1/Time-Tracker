<?php
require_once '../../security.php';
require_once '../../database.php';

$get_projets = getAllProjets();
$get_clients = getAllClients();
?>

<?php require_once '../../layout/header.php'; ?>

<h1>Paramètres Projets</h1>


<section class="add-projet">
	<h2>Créer nouveau Projet</h2>
	<form action="create-query.php" method="POST">

		<select name="pick-client">
			<option value="" disabled selected> Clients</option>
		    <?php foreach ($get_clients as $clients) : ?>
		        <option value="<?php echo $clients['id']; ?>"><?php echo $clients['nom']; ?></option>
		    <?php endforeach; ?>
	    </select>

		<input type="text" placeholder="Nom Projet" name="name-projet" required>
		<input type="submit" value="Créer">
	</form>
</section>

<section class="delete-projet">
	<h2>Supprimer Projet</h2>
	<form action="delete-query.php" method="POST" >

		<select name="delete_projet">
			<option value="" disabled selected> Clients</option>
		    <?php foreach ($get_projets as $projets) : ?>
		        <option value="<?php echo $projets['id']; ?>"><?php echo $projets['nom']; ?></option>
		    <?php endforeach; ?>
	    </select>

		<input type="submit" value="Supprimer Projet">
	</form>
</section>

<section class="update-projet">
	<h2>Modifier Nom Projet</h2>
	<form action="update-query.php" method="POST" >

		<select name="id-projet">
			<option value="" disabled selected> Projets</option>
		    <?php foreach ($get_projets as $projets) : ?>
		        <option value="<?php echo $projets['id']; ?>"><?php echo $projets['nom']; ?></option>
		    <?php endforeach; ?>
	    </select>

	    <input type="text" name="nom-projet">

		<input type="submit" value="Modifier">
	</form>
</section>

<?php require_once '../../layout/footer.php'; ?>