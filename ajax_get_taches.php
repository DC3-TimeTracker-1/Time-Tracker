<?php

require_once 'database.php';

$projet_id = $_GET['id'];
$liste_taches = getAllTachesOfProjet($projet_id);

?>

<table>
    <thead>
        <tr>
	        <th>Tache</th>
	        <th>Catégorie</th>
	        <th>Temps</th>
        </tr>
	</thead>
    <tbody>
	<?php foreach ($liste_taches as $tache) : ?>
    	<tr>
        	<td><?php echo $tache['tache_nom']; ?></td>
        	<td><?php echo $tache['categorie_nom']; ?></td>
        	<td><?php echo $tache['total_tache']; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>