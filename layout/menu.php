<?php require_once dirname(__FILE__) . '/../parameters.php'; ?>

<nav class="container navbar navbar-default navbar-fixed-top col-xs-12">
    <ul>
    	<li><a href="<?php echo $website_root ?>"><img src="images/logo.png" alt="logo" class="logo col-xs-2"></a></li>
        <li><a href="<?php echo $website_root ?>" class="col-xs-1">Accueil</a></li>
        <li><a href="<?php echo $website_root ?>crud/client/forms.php" class="col-xs-1">Clients</a></li>
        <li><a href="<?php echo $website_root ?>crud/projet/forms.php" class="col-xs-1">Projets</a></li>
        <li><a class="client moncompte col-xs-1" href="<?php echo $website_root ?>crud/utilisateur/forms.php">Mon Compte</a></li>
        <li><a class="client logout col-xs-1" href="<?php echo $website_root ?>logout.php">Deconnexion</a></li>
    </ul>
</nav>