<?php

require_once 'parameters.php';

try {
    $connection = new PDO('mysql:dbname=' . $dbname . ';host=' . $host, $user, $password);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $connection->exec("SET names utf8");
    $connection->exec("SET lc_time_names = 'fr_FR';");
} catch (PDOException $exc) {
    die("Erreur de connexion à la base de données.");
}


$files = scandir(dirname(__FILE__) .'/model');

foreach ($files as $file) {
    if (!is_dir($file) && substr($file, 0, 1) != '.') {
        require_once 'model/' . $file;
    }
}