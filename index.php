<?php
require_once 'security.php';
require_once 'database.php';

$get_clients = getAllClients();

?>

<?php require_once 'layout/header.php'; ?>

<h1>Bonjour <?php echo $_SESSION['email']; ?></h1>

<section class="clients col-xs-5">
	<select name="all-clients" class="flow" id="clients-select">
		<option value="" disabled selected> Clients</option>
	    <?php foreach ($get_clients as $clients) : ?>
	        <option value="<?php echo $clients['id']; ?>"><?php echo $clients['nom']; ?></option>
	    <?php endforeach; ?>
    </select>
</section>

<section class="projets">
	<select name="all-projet-user" id="projets-select">
		<option value="" disabled selected> Projets</option>
    </select>
</section>



<section>
	<article class="timer">
		<input type="text" id="libelle-tache" placeholder="Libelle de la tache">
		<span id="hours">00</span>:<span id="minutes">00</span>:<span id="seconds">00</span>
		<input type="submit" value="START" id="start-timer" disabled>
		<input type="submit" value="PAUSE" id="pause-timer" disabled>
</article>
</section>

<section id="liste-taches">
	<!-- AJAX -->
</section>

<!-- A FAIRE BOUTON DEMARRER + TIMER : INSERT AU DEBUT + UPDATE A LA FIN -->
<?php require_once 'layout/footer.php';
