$(document).ready(function() {
	
	function loadListTaches(projet_id) {
		// Charger la liste des temps saisis
		$.ajax({
			method: "GET",
			url: "ajax_get_taches.php",
			data: {id: projet_id}
		}).done(function(html) {
			$('#liste-taches').html(html);
		});
	}

	function isProjetSelected() {
 		var projet_id = $('#projets-select').val();
 
 		if (!isNaN(parseInt(projet_id))) {
 			$('#start-timer').prop('disabled', false);
 			$('#pause-timer').prop('disabled', false);
			loadListTaches(projet_id);
 		} else {
 			$('#start-timer').prop('disabled', true);
 			$('#pause-timer').prop('disabled', true);
 		}
 	}

	$("#clients-select").change(function(event) {
		event.preventDefault();
		console.log($(this).val());

		$.ajax({
			method: "GET",
			url: "ajax_get_projets.php",
			data: {id:$(this).val()}
		}).done(function(data) {
			$("#projets-select").html(data);
			isProjetSelected();
		});
	});

	$("#projets-select").change(function(event) {
 		event.preventDefault();
 		isProjetSelected();
 	});

	// FONCTION TIMER //

	//"0" AVANT LES TEMPS SI < 10
	function timerCall ( val ) { 
		return val > 9 ? val : "0" + val; 
	}

	var Clock = {
	    totalSeconds: 00,
	    isStarted: false,

	    start: function () {
	        var self = this;
	        self.isStarted = true;

	        this.interval = setInterval(function () {
	            self.totalSeconds += 1;

	            $("#hours").text(timerCall(Math.floor(self.totalSeconds / 3600)));
	            $("#minutes").text(timerCall(Math.floor(self.totalSeconds / 60 % 60)));
	            $("#seconds").text(timerCall(parseInt(self.totalSeconds % 60)));
	        }, 1000);
	    },

	    pause: function () {
	        clearInterval(this.interval);
	        delete this.interval;
	    },

	    resume: function () {
	        if (!this.interval) this.start();
	    },

	    stop: function () {
	    	var self = this;
	    	self.isStarted = false;
	    	self.totalSeconds = 00;
	    	clearInterval(this.interval);
	        delete this.interval;
	        $("#hours").text("00");
	        $("#minutes").text("00");
	        $("#seconds").text("00");
	    }
	};

	var currentTacheId = null;

	$("#start-timer").click(function() {
		if(!Clock.isStarted){
			Clock.start();
			$("#start-timer").val("STOP");
			// Enregistrer en BDD la nouvelle tache et créer un temps associé
			var projet_id = $('#projets-select').val();
			var nom = $('#libelle-tache').val();
			$.ajax({
				method: "GET",
				url: "ajax_insert_tache.php",
				data: {nom: nom, id: projet_id},
				dataType: 'json'
			}).done(function(data) {
				currentTacheId = data.id;
			});
		}
		else{
			Clock.stop();
			$("#start-timer").val("START");
			// Mettre à jour la date de fin du temps en cours
			$.ajax({
				method: "GET",
				url: "ajax_update_temps.php",
				data: {id: currentTacheId}
			}).done(function(data) {
				var projet_id = $('#projets-select').val();
				loadListTaches(projet_id);
			});
		}
	});

	$('#pause-timer').click(function () {
    if (Clock.interval) {
        Clock.pause();
        $("#pause-timer").val("RESUME");
    } else { 
        Clock.resume();
        $("#pause-timer").val("PAUSE");
    }
});

});