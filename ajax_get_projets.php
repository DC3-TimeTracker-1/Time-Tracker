<?php

require_once 'database.php';

$client_id = $_GET['id'];
$get_projets_user = getAllProjetsOfClient($client_id);

// echo json_encode($get_projets_user);

?>

<option value="" disabled selected> Projets</option>
<?php foreach ($get_projets_user as $projets) : ?>
	<option value="<?php echo $projets['id']; ?>"><?php echo $projets['nom']; ?></option>
<?php endforeach; ?>