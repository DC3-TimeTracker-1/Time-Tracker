<?php

require_once 'database.php';

session_start();

$user = NULL;

if (!isset($_SESSION['email'])) {
    
    if (isset($_POST['email']) && isset($_POST['mot_de_passe'])) {
        $email = $_POST['email'];
        $mot_de_passe = $_POST['mot_de_passe'];
        
        $user = getUtilisateurAuth($email, $mot_de_passe);
        
        if ($user != NULL) {
            $_SESSION['email'] = $user['mail'];
            $_SESSION['user_id'] = $user['id'];
        }
    }
    
} else {
    $user = getUtilisateurByEmail($_SESSION['email']);
}

if ($user == NULL) {
    header('Location: login.php');
}